<?php

namespace Mpwar\FizzBuzz\Solver;

use Mpwar\FizzBuzz\Solver;

final class FizzSolver implements Solver
{
    const FIZZ_DIVIDEND = 3;
    const FIZZ_VALUE = 'fizz';

    public function composeStackedResult($inputNumber, $stackedResult)
    {
        if ($inputNumber % self::FIZZ_DIVIDEND !== 0) {
            return $stackedResult;
        }

        $formattedStackedResult = $stackedResult
            ? $stackedResult . ' '
            : '';

        return $formattedStackedResult . self::FIZZ_VALUE;
    }
}