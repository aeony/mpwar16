<?php

namespace Mpwar\FizzBuzz\Solver;

use Mpwar\FizzBuzz\Solver;

final class GenericSolver implements Solver
{
    public function composeStackedResult($inputNumber, $stackedResult)
    {
        return $stackedResult ? : (string) $inputNumber;
    }
}