<?php

namespace Mpwar\Doubles;

class Sut
{
    const NEGATIVE_NUMBER = 'negative';
    const PRIME_NUMBER = 'prime';

    private $primeNumberService;
    private $anotherService;

    public function __construct(PrimeNumberService $aPrimeNumberService, AnotherService $anotherService)
    {
        $this->primeNumberService = $aPrimeNumberService;
        $this->anotherService = $anotherService;
    }

    public function doSomething($number)
    {
        if ($number < 0) {
            return self::NEGATIVE_NUMBER;
        }

        if ($this->primeNumberService->isPrime($number)) {
            return self::PRIME_NUMBER;
        }

        return $this->anotherService->getNumberType($number);
    }

    // Remember: private methods for the conditions!
}
