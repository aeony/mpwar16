Feature: I can detect fizz buzz numbers
    As a sane person
    I want to make sure that fizz buzz numbers can be recognised
    So that I can pass testing

    Scenario: default case
        Given a 15
        When executing the fizz buzz service
        Then the result should be 'fizz buzz'

    Scenario: another case
        Given a 30
        When executing the fizz buzz service
        Then the result should be 'fizz buzz'
