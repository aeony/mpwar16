<?php

namespace MpwarUnit\FizzBuzz\Solver;

use Mpwar\FizzBuzz\Solver\BuzzSolver;
use PHPUnit_Framework_TestCase;

final class BuzzSolverTest extends PHPUnit_Framework_TestCase
{
    const BUZZ = 'buzz';
    const EMPTY_STACKED_RESULT = '';
    const FILLED_UP_STACKED_RESULT = 'monesvol';

    protected function tearDown()
    {
        $this->number = null;
        $this->stackedResult = null;
    }

    /**
     * @test
     * @dataProvider numberDivisibleByFiveProvider
     */
    public function shouldReturnBuzzForNumbersThanCanBeDividedByFiveWithoutStackedResult($number)
    {
        $this->givenTheNumber($number);
        $this->andAnEmptyStackedResult();
        $this->thenTheResultIsBuzz();
    }

    /**
     * @test
     * @dataProvider numberDivisibleByFiveProvider
     */
    public function shouldReturnBuzzForNumbersThanCanBeDividedByFiveWithStackedResult($number)
    {
        $this->givenTheNumber($number);
        $this->andStackedResultWithAContent();
        $this->thenTheResultIsTheStackedResultPlusBuzz();
    }

    /**
     * @test
     * @dataProvider numberIsNotDivisibleByFiveProvider
     */
    public function shouldReturnTheStackedResultWhenTheNumberIsNotDivisibleByFive($number)
    {
        $this->givenTheNumber($number);
        $this->andStackedResultWithAContent();
        $this->thenTheResultIsTheStackedResult();
    }

    public function numberDivisibleByFiveProvider()
    {
        return [
            [5], [10], [15], [20], [25],
        ];
    }

    public function numberIsNotDivisibleByFiveProvider()
    {
        return [
            [2], [3], [4], [6], [7], [8], [9], [11],
        ];
    }

    private function givenTheNumber($number)
    {
        $this->number = $number;
    }

    private function andAnEmptyStackedResult()
    {
        $this->stackedResult = self::EMPTY_STACKED_RESULT;
    }

    private function andStackedResultWithAContent()
    {
        $this->stackedResult = self::FILLED_UP_STACKED_RESULT;
    }

    private function thenTheResultIsBuzz()
    {
        $result = $this->executeService();
        $this->assertSame(self::BUZZ, $result);
    }

    private function thenTheResultIsTheStackedResultPlusBuzz()
    {
        $result = $this->executeService();
        $this->assertSame(
            self::FILLED_UP_STACKED_RESULT . ' ' . self::BUZZ,
            $result
        );
    }

    private function thenTheResultIsTheStackedResult()
    {
        $result = $this->executeService();
        $this->assertSame(self::FILLED_UP_STACKED_RESULT, $result);
    }

    private function executeService()
    {
        $BuzzSolver = new BuzzSolver;

        return $BuzzSolver->composeStackedResult($this->number, $this->stackedResult);
    }
}